%%% ---------------------------------------------------------
%%% Utility theory
%%% ---------------------------------------------------------

%%% ---------------------------------------------------------
%%% Public rules
%%% ---------------------------------------------------------

:- public getConnector/1, getUtility/1, checkParameters/2.

%%% ---------------------------------------------------------
%%% Init
%%% ---------------------------------------------------------

:- $typeutils.addtypesearchpath("UnityActorSimulator", "UnityActorSimulator").

%%% ---------------------------------------------------------
%%% Handler utility methods
%%% ---------------------------------------------------------

changeID(OldName, NewName) :-
	checkParameters([OldName, NewName], [VOldName, VNewName]),
	getConnector(ConnectorListener),
	Handler is ConnectorListener.getHandlerFromID(VOldName),
	set_property(Handler, "ID", VNewName).

%%% ---------------------------------------------------------
%%% UnityActorSimulator utility methods
%%% ---------------------------------------------------------

getConnector(ConnectorListener) :-
	Connector is $typeutils.findtype("UnityActorSimulator.TcpConnectorListener"),
	ConnectorListener is $object.findobjectoftype(Connector).

getUtility(Utility) :-
	Utility is $typeutils.findtype("UnityActorSimulator.UnityPrologUtility").

%%% Simple patch in order to get the correct arguments value within the prolog goals
%%% that the java actor send.

checkParameters([], []).

checkParameters([Head | Tail], [Head | Tail2]) :-
	is_class(Head, $'System.String'),
	checkParameters(Tail, Tail2).

checkParameters([Head | Tail], [HeadStr | Tail2]) :-
	is_class(Head, $'Prolog.Symbol'),
	property(Head, name, HeadStr),
	checkParameters(Tail, Tail2).
